<?php

namespace app\controllers;

use \libs\Request;

abstract class AbstractController{

    protected $request;
    public function __construct( Request $request )
    {
        $this->request =    $request;
    }

    public abstract function view();

  
}
