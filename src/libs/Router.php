<?php
namespace libs;

Class Router {


    protected $controller ;


    protected $action ;

    
    private $request;


    public function __construct( $request )
    {
        $this->request = $request;

        $this->parse();
    }


    public function parse()
    {
        $resultParse   = parse_url( $this->request->getUri() );

        $explodeResult = explode( "/",trim($resultParse['path'],'/'));

        $this->controller =$explodeResult[0]; 
    
        if( isset( $explodeResult[1] ) )
        {
            $this->action       = $explodeResult[1];
        }
        else{
            $this->action       = 'view';
        }
        
    }


    public function getController()
    {
        return $this->controller;
    }


    public function getAction()
    {
        return $this->action;
    }

    public function getBase()
    {
        return $this->base;
    }

}