<?php

require_once "libs/autoLoad.php" ;

use \libs\Router;
use \libs\Request;
use app\controllers\CarController;
use app\controllers\BussController;
$request = new Request();

$uri =  new Router($request);



$action     =   $uri->GetAction();

$controller =   $uri->GetController();


switch ( strtolower( $controller ) ) 
{
    case 'car':
        $car = new CarController($request);
        $car->$action();
        
        break;
    case 'buss':
        $buss = new BussController($request);
        $buss->$action();
        
        break;
    default:
        
        break;
}