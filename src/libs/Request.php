<?php

namespace libs;

class Request 
{

    public function getUri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    public function getHttpHost()
    {
        return $_SERVER['HTTP_HOST'];
    }

    public function getUserIP()
    {
        return $_SERVER['REMOTE_ADDR'];
    }

    public function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function getQuery()
    {
        return $_SERVER['QUERY_STRING'];
    }

    public function getPostParams()
    {
        return $_POST;
    }
    public function getQueryParams()
    {
        return $_GET;
    }
    public function getParams()
    {
        return $_REQUEST;
    }
    
}