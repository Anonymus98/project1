<?php

namespace app\models;

class Car
{
    private $carList = array(
        array(
            "maker"=>"Mercedes",
            "model"=>"CLK",
            "horsePower"=>193,
            "color"=>"black",
            "year"=>2001
        ),
        array(
            "maker"=>"BMW",
            "model"=>"3series",
            "horsePower"=>201,
            "color"=>"blue",
            "year"=>2005
        ),
        array(
            "maker"=>"Mazda",
            "model"=>"6",
            "horsePower"=>230,
            "color"=>"blue",
            "year"=>2015
        )
    );
    
    public function GetRangeHp( $from,$to )
    {   
        $result = array();
        foreach ($this->carList as $car) 
        {
            if( $car['horsePower'] >= $from && $car['horsePower'] <= $to ) 
            {   
                array_push( $result,$car );
            }
        }
        return $result;
    }
}